import React, {Component} from "react";
import styles from "./todoItemList.module.css";

interface TodoItemProps {
    handleSubmit: any
    id: number
    title: string
}

interface TodoItemState {
    updateTitle: string
}

class TodoItem extends Component<TodoItemProps, TodoItemState> {
    state = {
        updateTitle: this.props.title
    }

    handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({updateTitle: e.target.value})
    }

    handleSubmit = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.handleSubmit(e.target.value, Number(e.target.id))
    }

    render() {
        return (
            <input
                className={styles.inputTodo}
                type="text"
                id={this.props.id.toString()}
                value={this.state.updateTitle}
                onChange={this.handleChange}
                onBlur={this.handleSubmit}
            />
        );
    }
}

export default TodoItem